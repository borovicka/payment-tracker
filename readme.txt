Assumptions:
-------------
- The file for loading initial balance contains only one record (row) for each currency code.
- If the file has invalid structure, application will run without loading file.
- If the user inserts invalid input, application will refuse this input and show how to insert input correctly.
- Exchange rates are loaded from KB public api, value in USD is shown only for currencies present in this api.

Project doesn't include unit tests.

Build application:
-------------------
This is standard maven project, use command: mvn clean install.

Run application:
-----------------
For run from commandline go to project folder and type:
java -jar target/payment-tracker.jar

You can optionally specify file for loading initial balances as the argument:
java -jar target/payment-tracker.jar initialBalances.data

For exit write "quit".