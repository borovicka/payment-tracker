package cz.jj.example;

import cz.jj.example.dto.PaymentRecord;
import cz.jj.example.io.Printer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

public class BalanceProvider {

    private Map<String, BigInteger> balances = new HashMap<>();;
    private Map<String, Float> rates;
    private boolean countRates;

    private static final String RATE_CURRENCY = "USD";
    private static final String HOME_CURRENCY = "CZK";
    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;

    public BalanceProvider() {
        countRates = false;
        this.rates = new HashMap<>();
    }

    public BalanceProvider(Map<String, Float> rates) {
        this.rates = rates;
        countRates = true;
    }

    public void setInitialBalances(Map<String, BigInteger> initialBalances) {
        balances = initialBalances;
    }

    public synchronized void processPayment(PaymentRecord paymentRecord) {

        String currencyCode = paymentRecord.getCurrencyCode();

        if(balances.containsKey(currencyCode)) {
            BigInteger balance = balances.get(currencyCode);
            BigInteger newBalance = balance.add(paymentRecord.getAmount());

            if(BigInteger.ZERO.compareTo(newBalance) == 0) {
                balances.remove(currencyCode);
            } else {
                balances.put(currencyCode, newBalance);
            }

        } else {

            if(BigInteger.ZERO.compareTo(paymentRecord.getAmount()) != 0) {
                balances.put(currencyCode, paymentRecord.getAmount());
            }

        }

        Printer.paymentProcessed();
    }

    public synchronized void printBalances() {
        Printer.printBalances();

        if(balances.isEmpty()) {

            Printer.noRecords();

        } else {

            if(countRates && rates.containsKey(RATE_CURRENCY)) {
                    BigDecimal baseRate = BigDecimal.valueOf(rates.get(RATE_CURRENCY));

                    balances.forEach((currencyCode, amount) -> {

                        if (HOME_CURRENCY.equals(currencyCode)) {

                            BigDecimal amountInDollars = new BigDecimal(amount).setScale(3, ROUNDING_MODE).divide(baseRate, ROUNDING_MODE);
                            Printer.printRecord(currencyCode, amount, amountInDollars.setScale(3, ROUNDING_MODE));

                        } else if (rates.containsKey(currencyCode)) {

                                Float rate = rates.get(currencyCode);
                                BigDecimal amountInDollars = new BigDecimal(amount)
                                        .multiply(BigDecimal.valueOf(rate))
                                        .divide(baseRate, ROUNDING_MODE)
                                        .setScale(3, ROUNDING_MODE);

                                Printer.printRecord(currencyCode, amount, amountInDollars);

                        } else {
                            Printer.printRecord(currencyCode, amount);
                        }
                    });


            } else {
                balances.forEach(Printer::printRecord);
            }

        }

        Printer.waitingForInput();
    }
}
