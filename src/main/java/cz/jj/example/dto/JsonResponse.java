package cz.jj.example.dto;

import java.util.List;

public class JsonResponse {

    private List<ExchangeRate> exchangeRates;

    public List<ExchangeRate> getExchangeRates() {
        return exchangeRates;
    }
}
