package cz.jj.example.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExchangeRate {

    @JsonProperty("currencyISO")
    private String currencyCode;
    @JsonProperty("middle")
    private Float rate;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Float getRate() {
        return rate;
    }
}
