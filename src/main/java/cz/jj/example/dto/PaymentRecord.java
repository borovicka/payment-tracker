package cz.jj.example.dto;

import java.math.BigInteger;

public class PaymentRecord {

    private String currencyCode;
    private BigInteger amount;

    public PaymentRecord(String currencyCode, BigInteger amount) {
        this.currencyCode = currencyCode;
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public BigInteger getAmount() {
        return amount;
    }
}
