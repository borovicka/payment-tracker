package cz.jj.example.util;

import cz.jj.example.dto.PaymentRecord;
import cz.jj.example.io.Printer;

import java.math.BigInteger;
import java.util.Optional;
import java.util.regex.Pattern;

public class Parser {

    private static Pattern pattern = Pattern.compile("^[A-Za-z]{3} -?[0-9]+");

    public static Optional<PaymentRecord> parseInput(String input) {
        if(Parser.isInvalid(input)) {
            Printer.invalidInput();
            return Optional.empty();
        }

        String currencyCode = input.substring(0, 3).toUpperCase();
        BigInteger amount = new BigInteger(input.substring(4));

        return Optional.of(new PaymentRecord(currencyCode, amount));
    }

    private static boolean isInvalid(String input) {
        return !pattern.matcher(input).matches();
    }

}
