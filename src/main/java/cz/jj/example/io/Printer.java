package cz.jj.example.io;

import cz.jj.example.Application;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;

public class Printer {

    public static void welcomeMessage() {
        System.out.println("Welcome in the payment tracker,");
        System.out.println(String.format("for exit write \"%s\".", Application.EXIT_COMMAND));
        System.out.println();
    }

    public static void invalidInput() {
        System.err.println("Invalid input");
        System.err.println("For payment record insert \"XXX [amount]\" where XXX is currency code");
        System.err.println("Decimal numbers are not supported for the amount");
    }

    public static void byeMessage() {
        System.out.println("Application will exit.");
        System.out.println("Bye");
    }

    public static void waitingForInput() {
        System.out.println();
        System.out.println("Waiting for the input: ");
        System.out.print("$");
    }

    public static void error() {
        System.err.println("Unexpected exception occurred while reading input, application exits");
    }

    public static void loadingFileError(Path path) {
        System.err.println(String.format("Cannot load file %s, initial balances are 0", path.toAbsolutePath()));
    }

    public static void printBalances() {
        System.out.println();
        System.out.println();
        System.out.println("Balance status:");
    }

    public static void noRecords() {
        System.out.println("No records available");
    }

    public static void printRecord(String currencyCode, BigInteger balance) {
        System.out.println(String.format("%s: %s", currencyCode, balance));
    }

    public static void printRecord(String currencyCode, BigInteger balance, BigDecimal amountInDollars) {
        System.out.println(String.format("%s: %s (USD %s)", currencyCode, balance, amountInDollars));
    }

    public static void paymentProcessed() {
        System.out.println();
        System.out.println("Payment has been processed");
        System.out.println();
    }
}
