package cz.jj.example.io;


import cz.jj.example.Application;
import cz.jj.example.BalanceProvider;
import cz.jj.example.dto.PaymentRecord;
import cz.jj.example.util.Parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;

public class ConsoleReader {

    private BalanceProvider balanceProvider;

    public ConsoleReader(BalanceProvider balanceProvider) {
        this.balanceProvider = balanceProvider;
    }

    public void read() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input;
        while(true) {

            try {
                input = reader.readLine();
            } catch (IOException e) {
                Printer.error();
                return;
            }

            if(Application.EXIT_COMMAND.equals(input)) {
                Printer.byeMessage();
                return;
            }

            Optional<PaymentRecord> paymentRecordOptional = Parser.parseInput(input);
            paymentRecordOptional.ifPresent(paymentRecord -> balanceProvider.processPayment(paymentRecord));

            Printer.waitingForInput();
        }
    }



}
