package cz.jj.example.io;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.jj.example.dto.ExchangeRate;
import cz.jj.example.dto.JsonResponse;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class ExchangeRateProvider {

    private Optional<List<ExchangeRate>> initRates() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonResponse[] jsonResponse = objectMapper.readValue(new URL("http://api.kb.cz/openapi/v1/exchange-rates"), JsonResponse[].class);
            if(jsonResponse!= null && jsonResponse.length > 0) {
                return Optional.of(jsonResponse[0].getExchangeRates());
            }
        } catch (IOException e) {
//            DO NOTHING (return empty optional)
        }

        return Optional.empty();
    }

    public Optional<Map<String, Float>> getExchangeRates() {
        return initRates().map(
                rates -> rates.stream()
                        .collect(Collectors.toMap(ExchangeRate::getCurrencyCode, ExchangeRate::getRate)));
    }

}
