package cz.jj.example;

import cz.jj.example.dto.PaymentRecord;
import cz.jj.example.exception.InvalidFileStructure;
import cz.jj.example.io.ConsoleReader;
import cz.jj.example.io.ExchangeRateProvider;
import cz.jj.example.util.Parser;
import cz.jj.example.io.Printer;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {

    public static final String EXIT_COMMAND = "quit";

    public static void main(String[] args) {
        Printer.welcomeMessage();
        Optional<Map<String, Float>> exchangeRates = new ExchangeRateProvider().getExchangeRates();

        BalanceProvider balanceProvider = exchangeRates.map(BalanceProvider::new).orElseGet(BalanceProvider::new);
        initBalances(args, balanceProvider);

        prepareTimer(balanceProvider);

//        sleep until balance output is written to console
        try {
            Thread.sleep(500L);
        } catch (InterruptedException e) {
//            DO NOTHING
        }

        new ConsoleReader(balanceProvider).read();
    }

    private static void initBalances(String[] args, BalanceProvider balanceProvider) {

        if(args.length > 0) {

            Path path = Paths.get(args[0]);

            if(Files.exists(path) && Files.isRegularFile(path)) {

                try(Stream<String> stream = Files.lines(path)) {

                    Map<String, BigInteger> initialBalances = stream
                            .map(Parser::parseInput)
                            .map(recordOptional -> recordOptional.<InvalidFileStructure>orElseThrow(InvalidFileStructure::new))
                            .collect(Collectors.toMap(PaymentRecord::getCurrencyCode, PaymentRecord::getAmount));

                    balanceProvider.setInitialBalances(initialBalances);
                    return;

                } catch (IOException | InvalidFileStructure | IllegalStateException e) {
//                    DO NOTHING - printer will write error, balances are set to zero
                }

            }

            Printer.loadingFileError(path);
        }

    }

    private static void prepareTimer(BalanceProvider balanceProvider) {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                balanceProvider.printBalances();
            }
        };

        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(task, 0L, 60000L);
    }
}
